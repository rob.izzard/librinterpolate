# librinterpolate

librinterpolate: a library to perform linear interpolation on a (mostly) 
constant gridded dataset. 

Current version: 1.11

(c) Robert Izzard 2005-2022

Usage:

The table should be organised (in memory) like this

```
p1[0] p2[0] p3[0] ... d1[0] d2[0] d3[0] d4[0] ...
p1[1] p2[1] p3[1] ... d1[1] d2[1] d3[1] d4[1] ...
p1[2] p2[2] p3[2] ... d1[2] d2[2] d3[2] d4[2] ...
```

so there are `n` parameters (`p1, p2, p3...`) and `d` data items
per data line (`d1, d2, d3, d4 ...`). There are `l` lines of data.

The parameters should be ordered from low to high values, although 
librinterpolate can sort a table for you, see `rinterpolate_sort_table`
below.

The parameters should be on a constant, perpendicular grid which 
does NOT need to be regular. As of V1.9 librinterpolate can take, 
and sort, and map a table that is not in the above form to an orthogonal 
table. The algorithm works best on data that is easily transformed, e.g. 
trapezium-like shapes. See `interpolate_map_right_columns()` below 
for more details.

What does this mean then?

This is a good data table:

```
0.1 -100 10 ...data...
0.1 -100 25 ...data...
0.1 -100 30 ...data...
0.1  -50 10 ...data...
0.1  -50 25 ...data...
0.1  -50 30 ...data...
0.3 -100 10 ...data...
0.3 -100 25 ...data...
0.3 -100 30 ...data...
0.3  -50 10 ...data...
0.3  -50 25 ...data...
0.3  -50 30 ...data...
0.9 -100 10 ...data...
0.9 -100 25 ...data...
0.9 -100 30 ...data...
0.9  -50 10 ...data...
0.9  -50 25 ...data...
0.9  -50 30 ...data...
```


In the above case, the parameters have values:
`p0` : `0.1,0.3,0.9`
`p1` : `-100, -50`
`p2` : `10,25,30`

The parameter "hypercube" then is the 3D-hypercube of
323 = 18 data lines.

Note that the points on the cube are constant but not regularly spaced,
e.g. `p0` has spacing `(0.3-0.1)=0.2` and `(0.9-0.3)=0.6`, which are different,
BUT e.g. the spacing for `p1` (`-100 - -50 = -50`) is the same, whatever the
value of `p0`. The same is true for `p3` which always has spacings `15` and `5`
from `(25-10)` and `(30-25)` respectively.

Note that the table is assumed to be sorted from <em>smallest</em> 
to <em>largest</em> parameter values. The data are to be regularly spaced 
and fully filled - no missing data please.

To interpolate data, `n` parameters are passed into this
routine using the array `x`. The result of the interpolation is put
into the array `r` (of size `d`).

If you enable `RINTERPOLATE_CACHE` (set on by default) then results are 
cached to avoid slowing the code too much.  This means
that the interpolate routine checks your input parameters `x` against
the last few sets of parameters passed in. If you have used these `x`
recently, the result is simply returned. This saves extra interpolation
and is often faster.  This is only true in some cases of course - if your
`x` are always different you waste time checking the cache. This is why
the cache_hint variable exists: if this is false then the cache is skipped.
Of course only you know if you are likely to call the interpolate routine
repeated with the same values... I cannot possibly know this in advance!

The interpolation process involves finding the lines of the data table
which span each parameter `x`. This makes a hypercube of length `2^n` (e.g.
in the above it is `8`, for simple 1D linear interpolation it would be the
two spanning values). Linear interpolation is then done in the largest
dimension, above this is the third parameter (`p2`), then the second (`p1`)
and finally on the remaining variable (`p0`). This would reduce the table
from `2^3` lines to `2^2` to `2^1` to (finally) `2^0` i.e. one line which is the
interpolation result.

To find the spanning lines a binary search is performed. This code was 
originally donated by Evert Glebbeek. See e.g.
http://en.wikipedia.org/wiki/Binary_search_algorithm
and note the comment "Although the basic idea of binary search is comparatively
straightforward, the details can be surprisingly tricky... ".

Each table has its own unique `table_id` number. This is just to allow
us to set up caches (one per table) and save arrays such as varcount and
steps (see below) so they only have to be calculated once.
Since <em>binary_c</em> V2.0 these `table_id` numbers have been allocated 
automatically, you do not have to set one yourself, but this does assume 
that the tables are each at fixed memory locations - so please do not move 
the data around once you start to use it.

Example code is given in `test_rinterpolate.c` and reproduced here

```
    #include "rinterpolate.h"
    
    /* Number of parameters */
    const rinterpolate_counter_t N = 2;

     /* Number of data */
    const rinterpolate_counter_t D = 3;

    /* length of each line (in doubles) i.e. N+D */
    const rinterpolate_counter_t ND = N + D;

    /* total number of lines */
    const rinterpolate_counter_t L = 100;

    /* make rinterpolate data (for cache etc.) */
    struct rinterpolate_data_t * rinterpolate_data = NULL;
    rinterpolate_counter_t status = rinterpolate_alloc_dataspace(&rinterpolate_data);

    /* data table : it is up to you to set the data in here*/
    rinterpolate_float_t table[ND*L];

    /* ... set the data ... */


    /*
     * Arrays for the interpolation location and
     * interpolated data. You need to set
     * the interpolation location, x.
     */
    rinterpolate_float_t x[N],r[D];

    /* ... set the x array ... */

    /* choose whether to cache (0=no, 1=yes) */
    int usecache = 1;

    /* do the interpolation */
    rinterpolate(table,
                 rinterpolate_data,
                 N,
                 D,
                 L,
                 x,
                 r,
                 usecache);

    /* the array r contains the result */


    /* ... rest of code ... */


    /* free memory on exit */
    rinterpolate_free_data(rinterpolate_data);
    free(rinterpolate_data);

```

If you have a dataset with a non-constantly-spaced final column,
you can map it to a constantly-spaced column with something like in the 
following example. The new table, which is assumed to have three columns,
has columns `1` and `2` (remember we start counting at `0`) mapped to `10` 
and `20` data points respectively.

```
struct rinterpolate_table_t * disordered_table = { 
    ... /* non-constantly spaced final column data */ 
    };

struct rinterpolate_table_t * ordered_table = 
    rinterpolate_map_right_columns(rinterpolate_data,
                                   disorded_table,
                                   (rinterpolate_Boolean_t []){
                                        /* which columns to map */
                                        FALSE, TRUE, TRUE
                                   },
                                   (rinterpolate_float_t []){
                                        /* mapped resolution */
                                        0,10,20
                                   },
                                   NULL); 
                                                                            
/* You can now interpolate on ordered_table */
```

The final argument to `rinterpolate_map_right_columns()` is `NULL`, which means the 
data points in the ordered_table are regularly spaced. You can also pass in an
array of arrays of type `rinterpolate_float_t` to tell the function
where to put the mapped points. These should have values between 0 and 1.

If you have a table that is not sorted, you can make a new sorted
version of the table with

```    
struct rinterpolate_table_t * const sorted_table = 
    rinterpolate_sort_table(rinterpolate_data,
                            unsorted_table);
```

or you can sort in place with this:

```
    rinterpolate_sort_table_in_place(rinterpolate_data,
                                     original_table);
```

You can swap columns in a table with:
```
    rinterpolate_swap_columns(rinterpolate_data,
                              table,
                              col1,
                              col2,
                              sort);
```
where `col1` and `col2` are the column numbers to be swapped
(remember these start at `0`) and `sort` is a boolean which 
you should usually set to `TRUE` because otherwise the resulting
swapped-column table will be out of order.

You can check if a table conforms librinterpolate's orthogonal
requirements using:

```
    rinterpolate_Boolean_t is_orthogonal = 
            rinterpolate_is_table_orthogonal(rinterpolate_data,
                                             table);
```


I have optimized this library as best I can, please let me know if
you can squeeze any more speed out of the function.
I am sorry this has made most of the function unreadable! The
comments should help, but you will need to know some tricks...

(c) Robert Izzard, 2005-2022, please send bug fixes!

Installation
============

You will need a C compiler, e.g. `gcc`, `meson`
(https://mesonbuild.com/) and `ninja` (https://ninja-build.org/).
If meson and ninja are not available as packages on your
system, try something like the following from a terminal:

~~~bash
pip3 install meson
pip3 install ninja
~~~


Then, use git to clone librinterpolate from its repository, e.g.

~~~bash
git clone https://gitlab.com/rob.izzard/librinterpolate.git
~~~

and then, from the librinterpolate directory, run:

~~~bash
meson setup --prefix=$HOME --buildtype=release builddir
ninja -C builddir install
~~~

which assumes your installation directories are in `$HOME` (e.g. `$HOME/bin`, `$HOME/lib`, etc.).

librinterpolate has been tested with modern versions of gcc (V10+) and clang (V11+).

gcc is available to all from https://gcc.gnu.org/ and clang is at https://clang.llvm.org/
