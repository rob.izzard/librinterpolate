#!/bin/bash

# find the version number of the command in the first
# argument : requires the command and sed

CMD=$1
VERSION_ARGS=$2
VCMD="$CMD $VERSION_ARGS"

exists()
{
  command -v "$1" >/dev/null 2>&1
}

if exists perl; then
    # perl regex: most powerful
    echo `$VCMD` 2>&1 | perl -ne 'print (/(\d+(?:\.\d+)+)/)'
else
    # less powerful sed regex
    echo `$VCMD` 2>&1 | sed -nre 's/^[^0-9]*(([0-9]+\.)*[0-9]+).*/\1/p'
fi

