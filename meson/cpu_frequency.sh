#!/bin/bash
#
######################
#
# Purpose:
#
# script to show maximum CPU frequency in Mhz, or return 1000 (Mhz)
# if not found
#
######################
#
# Originally part of the binary_c project
# https://gitlab.com/binary_c
# please see its LICENCE for usage terms and conditions.
#
######################

SYSFILE="/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq";

if [ -f "$SYSFILE" ]; then
    # standard on Linux
    gawk "{print int(\$1/1000.0)}" $SYSFILE || exit 1

elif [ "$(sysctl >/dev/null 2>/dev/null)" ]; then
    # this might work on a Mac
    sysctl -a |gawk "{print \$2}" || exit 1

elif [ -f /proc/cpuinfo ]; then

    # crude backup: burst the CPU and look at cpuinfo
    #echo "Using /proc/cpuinfo"
    seq 4 | xargs -P0 -n1 timeout 0.5 yes > /dev/null || \
    grep -i Mhz /proc/cpuinfo | gawk "{print int(\$4)}"|head -1 || exit 1

else
    # default to 1000 Mhz
    echo 1000
fi
