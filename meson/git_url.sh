#!/bin/bash
cd ${MESON_SOURCE_ROOT}

# wrapper to output the git url
#git config --get remote.origin.url

git status 2>/dev/null >/dev/null
GIT=$?
if [ $GIT == 0 ]; then
   git config --get remote.origin.url 2>/dev/null
fi
