#!/bin/bash
######################
#
# Purpose:
#
# return a string that determines if we're on a virtual machine
#
# This is ONLY for Linux
#
# echo "none" if not a VM, otherwise a string that
# describes the VM (see "systemd-detect-virt --list"
# for a list)
#
######################
#
# Originally part of the binary_c project
# https://gitlab.com/binary_c
# please see its LICENCE for usage terms and conditions.
#
######################


VIRT=`systemd-detect-virt 2>/dev/null`
if [ -z $VIRT ]; then
    echo "none"
else
    echo $VIRT
fi
