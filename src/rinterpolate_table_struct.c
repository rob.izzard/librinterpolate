#include "rinterpolate.h"

struct rinterpolate_table_t * rinterpolate_table_struct(struct rinterpolate_data_t * rinterpolate_data,
                                                        const void * const d,
                                                        const char * label)
{
    /*
     * Search the list of rinterpolate tables for one
     * whose data == d or label matches that passed in.
     *
     * Return its struct, if found, otherwise NULL.
     *
     * Set d or label to NULL to ignore them.
     */

    const int n = rinterpolate_table_index(rinterpolate_data,
                                           d,
                                           label);
    return n==-1
        ? NULL
        : rinterpolate_data->tables[n];
}
