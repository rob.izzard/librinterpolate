#include "rinterpolate.h"
#include "rinterpolate_internal.h"

struct rinterpolate_table_t * rinterpolate_linear_table(
    struct rinterpolate_data_t * const rinterpolate_data,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_float_t * const min,
    const rinterpolate_float_t * const max,
    const rinterpolate_signed_counter_t * const nx,
    rinterpolate_float_t (*func)(const rinterpolate_float_t * const,
                                 const rinterpolate_counter_t,
                                 const rinterpolate_counter_t)
    )
{
    /* counter number of lines */
    rinterpolate_counter_t l = 1;
    for(rinterpolate_counter_t i = 0; i<n; i++)
    {
        l *= abs(nx[i]);
    }

    /* make coordinate lists */
    rinterpolate_float_t ** x = Rinterpolate_calloc(n,sizeof(rinterpolate_float_t));
    if(unlikely(x == NULL))
    {
        rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                           "Failed to calloc x for linear table of size %zu * %zu = %zu\n",
                           rinterpolate_data,
                           (size_t)n,
                           sizeof(rinterpolate_float_t),
                           ((size_t)n) * sizeof(rinterpolate_float_t));
    }

    for(rinterpolate_counter_t i = 0; i<n; i++)
    {
        rinterpolate_counter_t _nx = (rinterpolate_counter_t)abs(nx[i]);
        if(_nx>0)
        {
            x[i] = Rinterpolate_malloc(sizeof(rinterpolate_float_t)*_nx);
            if(unlikely(x[i] == NULL))
            {
                rinterpolate_error(RINTERPOLATE_MALLOC_FAILED,
                                   "Failed to malloc x[i=%u] for linear table of size %zu\n",
                                   rinterpolate_data,
                                   i,
                                   n * sizeof(rinterpolate_float_t));
            }
            for(rinterpolate_counter_t j=0; j<_nx; j++)
            {
                x[i][j] =
                    nx[i] <=1 ? 0.5 :
                    ((rinterpolate_float_t)j/((rinterpolate_float_t)(_nx - 1))) * (max[i] - min[i]);
            }
        }
    }

    rinterpolate_float_t * const table_data =
        Rinterpolate_calloc((n+d)*l,sizeof(rinterpolate_float_t));

    const size_t vector_length = sizeof(rinterpolate_float_t) * (size_t)n;
    rinterpolate_float_t * vector = Rinterpolate_malloc(vector_length);
    rinterpolate_counter_t line = 0;
    rinterpolate_counter_t * step = Rinterpolate_calloc(1,sizeof(rinterpolate_counter_t) * n);
    while(line < l)
    {
        rinterpolate_signed_counter_t col = n-1;
        while(line>0 && col>=0)
        {
            step[col]++;
            if(step[col] >= (rinterpolate_counter_t)abs(nx[col]))
            {
                step[col--] = 0;
            }
            else
            {
                break;
            }
        }

        /* parameters */
        for(rinterpolate_counter_t i = 0; i < n; i++)
        {
            *(vector + i) =
                nx[i] > 0 ?
                x[i][step[i]] :
                rinterpolate_random_coordinate(i);
        }

        rinterpolate_float_t * p = table_data + line * (n+d);
        memcpy(p,vector,vector_length);
        p += n;

        /* data */
        for(rinterpolate_counter_t i = 0; i<d; i++)
        {
            *(p++) = func(vector,n,i);
        }

        line++;
    }

    const rinterpolate_counter_t table_number =
        rinterpolate_add_new_table(
            rinterpolate_data,
            table_data,
            n,
            d,
            l,
            0,
            FALSE);

    struct rinterpolate_table_t * const table =
        rinterpolate_data->tables[table_number];

    table->auto_free_data = TRUE;

    for(rinterpolate_counter_t i = 0; i<n; i++)
    {
        Safe_free(x[i]);
    }
    Safe_free(x);
    Safe_free(vector);
    Safe_free(step);
    return table;
}
