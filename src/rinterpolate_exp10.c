#include "rinterpolate.h"
#include "rinterpolate_internal.h"
/*
 * provide exp10() for OSX etc.
 */

#ifndef RINTERPOLATE_HAVE_NATIVE_EXP10

double exp10(double x)
{
    return pow(10.0,x);
}

#endif // __HAVE_NATIVE_EXP10
