#include "rinterpolate.h"
#include "rinterpolate_internal.h"

/*
 * Given a pointer to a table, find it in rinterpolate_data
 * and remove it from the list of tables.
 *
 * This function does NOT remove references to any
 * nested min_max table.
 */

void rinterpolate_free_table_ref(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                 struct rinterpolate_table_t * const table)
{

    if(rinterpolate_data != NULL &&
       table != NULL)
    {
        for(rinterpolate_counter_t i=0;i<rinterpolate_data->number_of_interpolation_tables;i++)
        {
            struct rinterpolate_table_t * const t = rinterpolate_data->tables[i];
            if(likely(t != NULL) &&
               unlikely(t == table))
            {
                rinterpolate_data->tables[i] = NULL;
            }
        }
    }
}
