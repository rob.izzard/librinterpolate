#include "rinterpolate.h"

int rinterpolate_table_index(struct rinterpolate_data_t * rinterpolate_data,
                             const void * const d,
                             const char * label)
{
    /*
     * Search the list of rinterpolate tables for one
     * whose data == d or label matches that passed in.
     *
     * Return the index of a matching table, or -1 if not found.
     *
     * Set d or label to NULL to ignore them.
     */
    if(d != NULL || label != NULL)
    {
        for(rinterpolate_counter_t i=0;
            i<rinterpolate_data->number_of_interpolation_tables;
            i++)
        {
            struct rinterpolate_table_t * const t = rinterpolate_data->tables[i];
            if(likely(t != NULL) &&
               (
                   unlikely(d!=NULL && t->data == d)
                   ||
                   unlikely(label != NULL && strcmp(t->label,label)==0)
                   )
                )
            {
                return (int)i;
            }
        }
    }
    return -1;
}
