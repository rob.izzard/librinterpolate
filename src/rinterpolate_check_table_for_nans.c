#include "rinterpolate.h"
#include "rinterpolate_internal.h"
#ifdef RINTERPOLATE_NANCHECKS

rinterpolate_Boolean_t rinterpolate_check_table_for_nans(struct rinterpolate_table_t * RESTRICT const table)
{
    /*
     * Check table for nan : if found return TRUE, if not
     * found return FALSE.
     */
    if(table->l > 0)
    {
        /*
         * Loop over lines
         */
        for(rinterpolate_counter_t l = 0;
            l < table->l;
            l++)
        {
            rinterpolate_float_t * const line = table->data + table->line_length * l;
            for(rinterpolate_counter_t j = 0;
                j < table->n;
                j++)
            {
                if(isnan(*(line + j)))
                {
                    printf("NaN found in table %p at line %u, parameter %u\n",
                           (void*)table,
                           l,
                           j);
                    return TRUE;
                }
            }
            rinterpolate_float_t * const linedata = line + table->n;
            for(rinterpolate_counter_t j = 0;
                j < table->d;
                j++)
            {
                if(isnan(*(linedata + j)))
                {
                    printf("NaN found in table %p at line %u, data item %u\n",
                           (void*)table,
                           l,
                           j);
                    return TRUE;
                }
            }
        }
    }
    return FALSE;
}
#endif//RINTERPOLATE_NANCHECKS
