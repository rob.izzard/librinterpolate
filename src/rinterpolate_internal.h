#pragma once
#ifndef RINTERPOLATE_INTERNAL_H
#define RINTERPOLATE_INTERNAL_H


/*************************************************************
 * librinterpolate's internal macros
 *************************************************************/

#ifndef FALSE
#define FALSE (false)
#endif

#ifndef TRUE
#define TRUE (true)
#endif

/*
 * Floating point range
 */
#define RINTERPOLATE_FLOAT_MAX (DBL_MAX)
#define RINTERPOLATE_FLOAT_MIN (DBL_MIN)

/* Convert a C (integer) expression to rinterpolate_Boolean_t type */
#ifndef Boolean_
#define Boolean_(EXPR) ((EXPR) ? (TRUE) : (FALSE))
#endif

/* fast bit-shift equivalent to pow(2,A) but for integers */
#ifndef Integer_power_of_two
#define Integer_power_of_two(A) (1<<(A))
#endif

/* macros to define less and more than operations */
#ifndef Less_than
#define Less_than(A,B) ((A)<(B))
#endif

#ifndef More_than
#define More_than(A,B) ((A)>(B))
#endif

#undef Concat3
#define Concat3(A,B,C) A##B##C

#ifndef Max
#define Max(A,B) Max_implementation(__COUNTER__,A,B)
#define Max_implementation(LINE,A,B) __extension__          \
    ({                                                      \
        const Autotype(A) Concat3(Max,LINE,a) = (A);        \
        const Autotype(B) Concat3(Max,LINE,b) = (B);        \
        More_than(Concat3(Max,LINE,a),Concat3(Max,LINE,b))  \
            ? (Concat3(Max,LINE,a))                         \
            : (Concat3(Max,LINE,b));                        \
            })
#endif

#ifndef Min
#define Min(A,B) Min_implementation(__COUNTER__,A,B)
#define Min_implementation(LINE,A,B) __extension__          \
    ({                                                      \
        const Autotype(A) Concat3(Min,LINE,a) = (A);        \
        const Autotype(B) Concat3(Min,LINE,b) = (B);        \
        Less_than(Concat3(Min,LINE,a),Concat3(Min,LINE,b))  \
            ? (Concat3(Min,LINE,a))                         \
            : (Concat3(Min,LINE,b));                        \
            })
#endif


#ifndef Force_range
#define Force_range(A,B,X) Force_range_implementation(LINE,A,B,X)
#define Force_range_implementation(LINE,A,B,X) __extension__            \
    ({                                                                  \
        const Autotype(A) Concat3(Force_range,LINE,a) = (A);            \
        const Autotype(B) Concat3(Force_range,LINE,b) = (B);            \
        const Autotype(X) Concat3(Force_range,LINE,x) = (X);            \
        (unlikely(Less_than(Concat3(Force_range,LINE,x),                \
                            Concat3(Force_range,LINE,a))))              \
            ? Concat3(Force_range,LINE,a) :                             \
            (unlikely(More_than(Concat3(Force_range,LINE,x),            \
                                Concat3(Force_range,LINE,b)))           \
             ? Concat3(Force_range,LINE,b)                              \
             : Concat3(Force_range,LINE,x));                            \
    })
#endif

#ifndef Is_zero
#define Is_zero(A) (fabs((A))<TINY)
#endif

#ifndef Fequal
#define Fequal(A,B) (Is_zero((A)-(B)))
#endif

#ifndef TINY
#define TINY (DBL_EPSILON)
#endif

#ifdef RINTERPOLATE_DEBUG
#  define FLUSH fflush(NULL);
#else
#  define FLUSH /* */
#endif // RINTERPOLATE_DEBUG

/*************************************************************
 * Compiler options
 *************************************************************/
#include "rinterpolate_compiler.h"



#endif // RINTERPOLATE_INTERNAL_H
