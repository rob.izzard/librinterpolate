#include "rinterpolate.h"
#include "rinterpolate_internal.h"

#define _random_number ((rinterpolate_float_t)rand()/RAND_MAX)

rinterpolate_float_t rinterpolate_random_coordinate(
    const rinterpolate_counter_t col
    )
{
    /*
     * Return a random coordinate in column col
     *
     * Note that these cannot overlap, so any coord
     * in col 0 cannot be in col 1, etc.
     */
    return (0.9 * _random_number + 0.1) * exp10((rinterpolate_float_t)col);
}
