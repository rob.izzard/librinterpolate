#include "rinterpolate.h"
#include "rinterpolate_internal.h"

struct rinterpolate_stats_t * rinterpolate_make_stats(
    const rinterpolate_counter_t n)
{
    /*
     * Build and return a stats struct and its contents.
     *
     * Return NULL on error.
     */
#define stats_malloc(X,SIZE)                    \
    stats->X = Rinterpolate_malloc(SIZE);       \
    if(stats->X==NULL) return NULL;
#define stats_calloc(X,N,SIZE)                  \
    stats->X = Rinterpolate_calloc(N,SIZE);     \
    if(stats->X==NULL) return NULL;

    struct rinterpolate_stats_t * const stats =
        Rinterpolate_malloc(sizeof(struct rinterpolate_stats_t));

    if(stats)
    {
        stats_malloc(valuelist,
                     n * sizeof(rinterpolate_float_t *));
        stats_calloc(nvalues,
                     n,
                     sizeof(rinterpolate_counter_t));
        stats_malloc(valuelist_alloced,
                     n * sizeof(rinterpolate_counter_t));
        stats_malloc(min,
                     n * sizeof(rinterpolate_float_t));
        stats_malloc(max,
                     n * sizeof(rinterpolate_float_t));
        stats_calloc(valuemax,
                     n,
                     sizeof(rinterpolate_float_t *));
        stats_calloc(valuemin,
                     n,
                     sizeof(rinterpolate_float_t *));
    }
    return stats;
}
