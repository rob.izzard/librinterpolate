#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_free_table_contents(struct rinterpolate_table_t * const table)
{
    /*
     * Free the contents of a rinterpolate table
     */
    if(table)
    {
        if(table->min_max_table)
        {
            for(rinterpolate_counter_t j=0;j<table->n;j++)
            {
                if(table->min_max_table[j] != NULL)
                {
                    rinterpolate_free_table_contents(table->min_max_table[j]);
                    Safe_free(table->min_max_table[j]);
                }
            }
            Safe_free(table->min_max_table);
        }
        rinterpolate_clear_stats(table);
        Safe_free(table->stats);
        if(table->hypertable)
        {
            rinterpolate_free_hypertable(table->hypertable);
        }
        if(table->presearch)
        {
            for(rinterpolate_counter_t j=0;j<table->presearch_n;j++)
            {
                Safe_free(table->presearch[j]);
            }
        }

        Safe_free(table->presearch);
        Safe_free(table->steps);
        Safe_free(table->varcount);
        Safe_free(table->hypertable);
        Safe_free(table->column_is_mapped);
#ifdef RINTERPOLATE_CACHE
        Safe_free(table->cache);
#endif//RINTERPOLATE_CACHE
        if(table->auto_free_data == TRUE)
        {
            Safe_free(table->data);
        }
    }
}
