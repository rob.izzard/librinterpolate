#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_alloc_varcount(struct rinterpolate_data_t * RESTRICT const rinterpolate_data MAYBE_UNUSED,
                                 struct rinterpolate_table_t * RESTRICT const table)
{
    /*
     * Allocate varcount array(s)
     */
    if(!table->varcount)
    {
        table->varcount = Rinterpolate_calloc(table->n,sizeof(rinterpolate_counter_t));
        if(unlikely(table->varcount==NULL))
        {
            rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                               "Error allocating varcount in table %p number %u\n",
                               rinterpolate_data,
                               (void*)table->parent,
                               table->table_number);
        }
    }

    rinterpolate_counter_t b = table->l;
    for(rinterpolate_counter_t j=0; j<table->n; j++)
    {
        table->varcount[j] = b/table->steps[j];
#ifdef RINTERPOLATE_DEBUG
        Rinterpolate_print("Interpolate debug: varcount[%u]=%u, b=%u, steps[%u]=%u\n",
                           j,table->varcount[j],b,j,table->steps[j]);
        FLUSH;
#endif
        b = table->steps[j];
    }
}
