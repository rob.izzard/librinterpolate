#include "rinterpolate.h"
#include "rinterpolate_internal.h"
#define _GNU_SOURCE
#include <stdlib.h>

void rinterpolate_sort_table_in_place(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                      struct rinterpolate_table_t * const table)
{
    /*
     * Sort table in place
     */
    struct rinterpolate_table_t * sorted_table =
        rinterpolate_sort_table(rinterpolate_data,
                                table);

    /*
     * free the old table struct's contents, but leave
     * the struct allocated in rinterpolate_data
     */
    rinterpolate_free_table_contents(table);

    /*
     * so we can copy over the top of it
     * with sorted_table (and set auto_free_data
     * because the copy is newly allocated)
     */
    memcpy(table,
           sorted_table,
           sizeof(struct rinterpolate_table_t));

    sorted_table->auto_free_data = TRUE;

    /*
     * and remove the reference to sorted_table from
     * rinterpolate_data's list
     */
    rinterpolate_free_table_ref(rinterpolate_data,
                                sorted_table);

    /*
     * Free sorted_table now it is copied into table
     */
    Safe_free(sorted_table);

    return;
}
