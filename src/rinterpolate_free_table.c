#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_free_table(struct rinterpolate_table_t ** table)
{
    /*
     * Free a table and its contents
     */
    rinterpolate_free_table_contents(*table);
    Safe_free(*table);
}
