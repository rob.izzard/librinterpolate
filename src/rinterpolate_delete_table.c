#include "rinterpolate.h"
#include "rinterpolate_internal.h"

rinterpolate_Boolean_t rinterpolate_delete_table(
    struct rinterpolate_data_t * const rinterpolate_data MAYBE_UNUSED,
    struct rinterpolate_table_t * const table,
    rinterpolate_float_t * data
    )
{
    /*
     * Delete table from rinterpolate_data,
     * or if table is NULL delete the table
     * with given data.
     *
     * We try to free the table contents also.
     *
     * Returns TRUE if a table is deleted,
     * FALSE otherwise.
     */

    if(table != NULL)
    {
        data = table->data;
    }

    if(data)
    {
        const rinterpolate_signed_counter_t old_table_id =
            rinterpolate_id_table(
                rinterpolate_data,
                data
                );
        if(old_table_id >= 0)
        {
            rinterpolate_free_table_contents(table);
            rinterpolate_data->tables[old_table_id] = NULL;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    else
    {
        return FALSE;
    }
}
