#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_swap_columns(struct rinterpolate_data_t * const rinterpolate_data,
                               struct rinterpolate_table_t * const table,
                               const rinterpolate_counter_t i,
                               const rinterpolate_counter_t j,
                               const rinterpolate_Boolean_t sort)
{
    /*
     * Swap columns i and j in table, in place.
     *
     * If sort is TRUE, resort the table.
     */
    rinterpolate_float_t * const ti = table->data + i;
    rinterpolate_float_t * const tj = table->data + j;
    for(rinterpolate_counter_t line = 0;
        line < table->l;
        line++)
    {
        const size_t ll = line * table->line_length;
        rinterpolate_float_t * const pi = ti + ll;
        rinterpolate_float_t * const pj = tj + ll;
        const rinterpolate_float_t tmp = *pj;
        *pj = *pi;
        *pi = tmp;
    }

#undef _swap
#define _swap(TYPE,VAR)                         \
    {                                           \
        if(table->VAR)                          \
        {                                       \
            TYPE tmp = table->VAR[i];           \
            table->VAR[i] = table->VAR[j];      \
            table->VAR[j] = tmp;                \
        }                                       \
    }

    _swap(struct rinterpolate_table_t * const, min_max_table);
    _swap(const rinterpolate_Boolean_t,column_is_mapped);
    _swap(const rinterpolate_counter_t,varcount);
    _swap(const rinterpolate_counter_t,steps);
    _swap(rinterpolate_float_t *,presearch);


    if(table->stats)
    {
#undef _swap
#define _swap(TYPE,VAR)                                     \
        {                                                   \
            TYPE tmp = table->stats->VAR[i];                \
            table->stats->VAR[i] = table->stats->VAR[j];    \
            table->stats->VAR[j] = tmp;                     \
        }
#define _swap_float_pointer(VAR) _swap(rinterpolate_float_t *,VAR)
#define _swap_float(VAR) _swap(const rinterpolate_float_t,VAR)
#define _swap_counter(VAR) _swap(const rinterpolate_counter_t,VAR)

        _swap_float_pointer(valuelist);
        _swap_float_pointer(valuemax);
        _swap_float_pointer(valuemin);
        _swap_float(max);
        _swap_float(min);
        _swap_counter(nvalues);
    }

    if(sort == TRUE)
    {
        /* sort in place */
        rinterpolate_sort_table_in_place(rinterpolate_data,
                                         table);
    }
    else
    {
        rinterpolate_analyse_table(rinterpolate_data,
                                   table,
                                   "swap columns");
    }

    /*
     * Cache is for the old column layout: remove it
     */
    Safe_free(table->cache);
    table->cache_length = 0;
}
