#ifdef __TEST_RINTERPOLATE__
#include "rinterpolate.h"
#include "rinterpolate_internal.h"
#ifdef RINTERPOLATE_HAVE_RDTSC
#ifdef _MSC_VER
#include <intrin.h>
#else
#ifdef RINTERPOLATE_HAVE_X86INTRIN_H
#include <x86intrin.h>
#endif
#endif //_MSC_VER
#endif//RINTERPOLATE_HAVE_RDTSC
#include <stdint.h>

#define RINTERPOOLATE_HAVE_RDTSC
typedef unsigned long long ticks;
ticks getticks(void);
#define _random_number ((rinterpolate_float_t)rand()/RAND_MAX)
#include <time.h>

/*
 * Test program for librinterpolate
 */

#define FIXED_TESTS 1
#define RANDOM_TESTS 1
#define SPIN_TESTS 1

static void test_free(struct rinterpolate_data_t ** rinterpolate_data);
static void test_exit(struct rinterpolate_data_t * rinterpolate_data);
static void test_rect(struct rinterpolate_data_t * rinterpolate_data);
static void test_map(struct rinterpolate_data_t * rinterpolate_data);
static void test_sort(struct rinterpolate_data_t * rinterpolate_data);
static void test_copy(struct rinterpolate_data_t * rinterpolate_data);
static void test_map2(struct rinterpolate_data_t * rinterpolate_data);

#define Report(TEST,TICKS)                              \
    printf("Report: %20s : %8.3f\n",TEST,TICKS);

void f(const rinterpolate_float_t u,
       const rinterpolate_float_t v,
       rinterpolate_float_t * RESTRICT x);

int main (int argc MAYBE_UNUSED,
          char **  argv MAYBE_UNUSED)
{
    struct rinterpolate_data_t * rinterpolate_data = NULL;

    /*
     * Verbosity.
     * 1 is normal.
     * 2 is more.
     */
    const rinterpolate_counter_t vb = 1;


#define TICKSCALE 100000000.0
    /*
     * Resolution multiplier (>=2).
     * NB errors are likely to be tiny when > 1000
     * (1000)
     */
#define NRES 1000

    /*
     * Number of tests to perform (10000000)
     */
#define NTESTS 10000000
//#define NTESTS 10


    /*
     * Number of tests when checking the cache
     */
#define NSPINTESTS 1000000
//#define NSPINTESTS 10


        /*
         * Use current time as seed for random generator
         */
    srand(time(0));

    /*
     * Number of parameters
     */
#define N 2
    /*
     * Number of data
     */
#define D 3

    /*
     * Number of lines of data types 1,2
     */
#define L1 (1*NRES)
#define L2 (4*NRES)

    /* length of each line (in doubles) i.e. N+D */
#define ND ((N)+(D))

    /* total number of lines */
#define L ((L1)*(L2))

#define COMPARE if(vb>=2)                                               \
                {                                                       \
                    printf("f(%g,%g)\n",x[0],x[1]);                     \
                    printf("Expected : %g %g %g\n",rr[0],rr[1],rr[2]);  \
                    printf("Got      : %g %g %g\n", r[0], r[1], r[2]);  \
                }


    rinterpolate_float_t table[ND*L];
    rinterpolate_counter_t i,j;

    /*
     * IDX = offset index of position i,j
     */
#define IDX(I,J) (((I)*L2+(J))*ND)

    /*
     * Fill the data table
     */
    for(i=0;i<L1;i++)
    {
        const rinterpolate_float_t u = (1.0*i)/(1.0*(L1-1));
        for(j=0;j<L2;j++)
        {
            const rinterpolate_float_t v = (1.0*j)/(1.0*(L2-1));
            const int offset = IDX(i,j);
            rinterpolate_float_t x[D];
            f(u,v,x);

            table[offset+0] = u;
            table[offset+1] = v;
            table[offset+2] = x[0];
            table[offset+3] = x[1];
            table[offset+4] = x[2];
        }
    }

    /*
     * Arrays for the interpolation location and
     * interpolated data.
     */
    rinterpolate_float_t x[N],r[D];

    /*
     * Allocate data space
     */
    const rinterpolate_counter_t status = rinterpolate_alloc_dataspace(&rinterpolate_data);
    rinterpolate_build_flags(rinterpolate_data);

    test_rect(rinterpolate_data);
    test_map(rinterpolate_data);
    test_sort(rinterpolate_data);
    test_copy(rinterpolate_data);
    test_exit(rinterpolate_data);


    if(status != 0)
    {
        printf("alloc data status != 0 = %u\n",status);
        fflush(NULL);
        exit(status);
    }

    /*
     * Calculations of the maximum difference between
     * the exact functions and the interpolated functions.
     */
    rinterpolate_float_t diffmax = 0.0;
#define DIFF(X,Y) (fabs((X)-(Y))/Min(fabs(X),fabs(Y)))
#define _MAXDIFF                                \
    Max(Max(diffmax,                            \
            DIFF(r[0],rr[0])),                  \
        Max(DIFF(r[1],rr[1]),                   \
            DIFF(r[2],rr[2])))

    if(status==0)
    {
        ticks tstart;

        /*
         * Fixed x
         */
        for(i=0;i<N;i++)
        {
            x[i] = _random_number;
        }

        rinterpolate_float_t t_cache = 0, t_nocache = 0;

        if(FIXED_TESTS)
        {
            printf("\nFixed input tests\n");
            /* without cache */
            diffmax = 0.0;
            tstart = getticks();
            for(i=0;i<NTESTS;i++)
            {
                rinterpolate(table,
                             rinterpolate_data,
                             N,
                             D,
                             L,
                             x,
                             r,
                             0);
                rinterpolate_float_t rr[D];
                f(x[0],x[1],rr);
                diffmax = _MAXDIFF;
                COMPARE;
            }
            t_nocache = (getticks() - tstart)/TICKSCALE;
            printf("%7s cache : %8.3f, maxdiff %6.4f %%\n","Without",t_nocache,100.0*diffmax);
            Report("Fixed nocache",t_nocache);

            /* with cache */
            tstart = getticks();
            diffmax = 0.0;
            for(i=0;i<NTESTS;i++)
            {
                rinterpolate(table,
                             rinterpolate_data,
                             N,
                             D,
                             L,
                             x,
                             r,
                             5);
                rinterpolate_float_t rr[D];
                f(x[0],x[1],rr);
                diffmax = _MAXDIFF;
                COMPARE;
            }
            t_cache = (getticks() - tstart)/TICKSCALE;
            printf("%7s cache : %8.3f, maxdiff = %6.4f %%\n","With",t_cache,100.0*diffmax);
            printf("Cache speed up : %5.3f %%\n",
                   100.0*((rinterpolate_float_t)t_nocache - (rinterpolate_float_t)t_cache)/
                   Min((rinterpolate_float_t)t_cache,(rinterpolate_float_t)t_nocache));
            Report("Fixed cache",t_cache);
        }


        if(RANDOM_TESTS)
        {
            printf("\nRandom input tests\n");
            /* without cache */
            diffmax = 0.0;
            tstart = getticks();
            for(i=0;i<NTESTS;i++)
            {
                for(j=0;j<N;j++)
                {
                    x[j] = _random_number;
                }
                rinterpolate(table,
                             rinterpolate_data,
                             N,
                             D,
                             L,
                             x,
                             r,
                             0);
                rinterpolate_float_t rr[D];
                f(x[0],x[1],rr);
                diffmax = _MAXDIFF;
                COMPARE;
            }
            t_nocache = (getticks() - tstart)/TICKSCALE;
            printf("%7s cache : %8.3f, maxdiff %6.4f %%\n","Without",t_nocache,100.0*diffmax);
            Report("Random nocache",t_nocache);

            /* with cache */
            diffmax = 0.0;
            tstart = getticks();
            for(i=0;i<NTESTS;i++)
            {
                x[0] = _random_number;
                x[1] = _random_number;
                rinterpolate(table,
                             rinterpolate_data,
                             N,
                             D,
                             L,
                             x,
                             r,
                             5);

                rinterpolate_float_t rr[D];
                f(x[0],x[1],rr);
                diffmax = _MAXDIFF;
                COMPARE;
            }
            t_cache = (getticks() - tstart)/TICKSCALE;
            Report("Random cache",t_cache);

            printf("%7s cache : %8.3f, maxdiff = %6.4f %%\n","With",t_cache,100.0*diffmax);
            printf("Cache speed up : %6.4f %%\n",
                   100.0*((rinterpolate_float_t)t_nocache - (rinterpolate_float_t)t_cache)/
                   Min((rinterpolate_float_t)t_cache,(rinterpolate_float_t)t_nocache));
        }



        if(SPIN_TESTS)
        {
            const rinterpolate_counter_t n = 5;
            printf("Spun input (n=%u) tests \n",n);
            double ** xx = malloc(n*sizeof(rinterpolate_float_t*));
            for(i=0;i<n;i++)
            {
                int kk;
                *(xx+i) = malloc(n*sizeof(rinterpolate_float_t));
                for(kk=0;kk<N;kk++)
                {
                    xx[i][kk] = _random_number;
                }
            }

            rinterpolate_counter_t ncache;
            const rinterpolate_counter_t maxncache = n*2;
            for(ncache=0; ncache < maxncache; ncache++)
            {
                diffmax = 0.0;
                tstart = getticks();
                for(i=0;i<NSPINTESTS;i++)
                {
                    memcpy(x,xx[i%n],sizeof(rinterpolate_float_t)*N);
                    rinterpolate(table,
                                 rinterpolate_data,
                                 N,
                                 D,
                                 L,
                                 x,
                                 r,
                                 ncache);
                    rinterpolate_float_t rr[D];
                    f(x[0],x[1],rr);
                    diffmax = _MAXDIFF;
                    COMPARE;
                }
                t_cache = (getticks() - tstart)/TICKSCALE;
                if(ncache==0)
                {
                    t_nocache = t_cache;
                }

                printf("%7s cache_length %2u : %8.3f, maxdiff = %6.4f %%\n","With",ncache,t_cache,100.0*diffmax);
                printf("                  Cache speed up : %5.3f %%\n",
                       100.0*((rinterpolate_float_t)t_nocache - (rinterpolate_float_t)t_cache)/
                       Min((rinterpolate_float_t)t_cache,(rinterpolate_float_t)t_nocache));
                char c[100];
                sprintf(c,"Spun %u",ncache);
                Report(c,t_cache);
            }

            for(i=0;i<n;i++)
            {
                free(xx[i]);
            }
            free(xx);
        }
    }

    test_free(&rinterpolate_data);
    return 0;
}

/*
 * Function used for testing
 */
void f(const rinterpolate_float_t u,
       const rinterpolate_float_t v,
       rinterpolate_float_t * RESTRICT x)
{
    x[0] = 10.0+sin(u)*cos(v);
    x[1] = 100.0+cos(v)*sin(u);
    x[2] = 1000.0+tan(u*v);
}





/*
 * Timer function
 */
ticks getticks(void) {

#ifdef RINTERPOLATE_HAVE_RDTSC
    /*
     * Use inbuilt __rdtsc function as a timer
     */
    return __rdtsc();
#else
    /*
     * Backup: use clock_gettime() to estimate the
     * number of "ticks".
     */
    struct timespec tp;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID,&tp);
    ticks t = (ticks) (
        ((uint64_t)tp.tv_sec  * (1000000000U * 1e-3 * CPUFREQ))
        +
        ((uint64_t)tp.tv_nsec * (1e-3 * CPUFREQ))
        );
    return t;
#endif // RINTERPOLATE_HAVE_RDTSC

}

static rinterpolate_float_t __function(const rinterpolate_float_t * const x,
                                       const rinterpolate_counter_t n,
                                       const rinterpolate_counter_t col);
static rinterpolate_float_t __function(const rinterpolate_float_t * const x,
                                       const rinterpolate_counter_t n,
                                       const rinterpolate_counter_t col)
{
    rinterpolate_float_t y = 1.0;
    for(rinterpolate_counter_t i=0; i<n; i++)
    {
        y *= sin(x[i]) + pow(10,(rinterpolate_float_t)col);
    }
    return y;
}

static void test_copy(struct rinterpolate_data_t * rinterpolate_data)
{

    /*
     * Test the copy function
     */
    enum{ n = 3 };
    enum{ d = 3 };
    rinterpolate_float_t min[n] = {
        0.0,0.0,0.0
    };
    rinterpolate_float_t max[n] = {
        10.0,20.0,30.0
    };
    const rinterpolate_signed_counter_t nx[n] = {
        3,3,3
    };
    struct rinterpolate_table_t * original_table
        = rinterpolate_linear_table(
            rinterpolate_data,
            n,
            d,
            min,
            max,
            nx,
            __function
            );

    struct rinterpolate_table_t * copy_table =
        rinterpolate_copy_table(rinterpolate_data,
                                original_table);

    rinterpolate_copy_table_data(original_table,
                                 copy_table);

}

static void test_sort(struct rinterpolate_data_t * rinterpolate_data)
{
    /*
     * Test the sort function
     */
    enum{ n = 3 };
    enum{ d = 3 };
    rinterpolate_float_t min[n] = {
        0.0,0.0,0.0
    };
    rinterpolate_float_t max[n] = {
        10.0,20.0,30.0
    };
    const rinterpolate_signed_counter_t nx[n] = {
        3,3,3
    };
    struct rinterpolate_table_t * original_table;
    original_table = rinterpolate_linear_table(
        rinterpolate_data,
        n,
        d,
        min,
        max,
        nx,
        __function
        );

    if(0)
        rinterpolate_show_table(rinterpolate_data,
                                original_table,
                                stdout,
                                -1);

    struct rinterpolate_table_t * t =
        rinterpolate_sort_table(rinterpolate_data,
                                original_table);
    rinterpolate_free_table_from_table(rinterpolate_data,
                                       t);

    rinterpolate_sort_table_in_place(rinterpolate_data,
                                     original_table);

    rinterpolate_free_table_from_table(rinterpolate_data,
                                       original_table);
}

static void test_map(struct rinterpolate_data_t * rinterpolate_data)
{

    /*
     * make random data table :
     * n = number of parameters,
     * d = number of data
     */
    enum{ n = 3 };
    enum{ d = 3 };



    /*
     * number of points in each dimension
     * if >0 : use this many from a fixed array
     * if <0 : use this many random numbers
     */
    const rinterpolate_signed_counter_t nx[n] = {3,3,3};

    struct rinterpolate_table_t * latest_table = NULL;
    struct rinterpolate_table_t * original_table;

    /*
     * Build the table
     */
    if(0)
    {
        original_table = rinterpolate_random_table(
            rinterpolate_data,
            n,
            d,
            nx,
            __function
            );
    }
    if(1)
    {
        rinterpolate_float_t min[n] = {
            0.0,0.0,0.0
        };
        rinterpolate_float_t max[n] = {
            10.0,20.0,30.0
        };
        original_table = rinterpolate_linear_table(
            rinterpolate_data,
            n,
            d,
            min,
            max,
            nx,
            __function
            );
        rinterpolate_show_table(rinterpolate_data,
                                original_table,
                                stdout,
                                -1);
    }

    /*
     * Make a sorted version of the table for later checks
     */
    struct rinterpolate_table_t * sorted_original_table =
        rinterpolate_sort_table(rinterpolate_data,
                                original_table);


    printf("original table\n");
    rinterpolate_show_table(rinterpolate_data,
                            original_table,
                            stdout,
                            10);

    /*
     * Map right columns
     */
    latest_table =
        rinterpolate_map_right_columns(rinterpolate_data,
                                       original_table,
                                       (rinterpolate_Boolean_t []){
                                           /* which columns to map */
                                           FALSE, TRUE, TRUE
                                       },
                                       (rinterpolate_float_t []){
                                           /* mapped resolution */
                                           3,3,3
                                       },
                                       NULL);

    printf(" ... is table %d\n",
           rinterpolate_id_table(rinterpolate_data,
                                 latest_table->data));
    rinterpolate_show_table(rinterpolate_data,
                            latest_table,
                            stdout,
                            10);

    /*
     * now test how accurate the mapping is
     */
    const rinterpolate_counter_t number_of_random_tests = 0;
    rinterpolate_counter_t random_counter = 0;
    double * r = Rinterpolate_malloc(d * sizeof(rinterpolate_float_t));

    while(random_counter++ < number_of_random_tests)
    {
        double * x = Rinterpolate_malloc(n * sizeof(rinterpolate_float_t));
        printf("Interpolate at : ");
        for(rinterpolate_counter_t i = 0; i<n; i++)
        {
            x[i] = rinterpolate_random_coordinate(i);
            printf("%g ",x[i]);
        }
        printf(" -> ");

        rinterpolate(
            latest_table->data,
            rinterpolate_data,
            latest_table->n,
            latest_table->d,
            latest_table->l,
            x,
            r,
            0);
        for(rinterpolate_counter_t i = 0; i<d; i++)
        {
            printf("%g ",r[i]);
        }
        printf("\n");
        Safe_free(x);
    }

    printf("\n\nTry to interpolate the mapped table using pre-mapped coordinates  \n\n\n");

    struct rinterpolate_table_t * coordinate_table =
        sorted_original_table;
    struct rinterpolate_table_t * table_to_interpolate =
        latest_table;

    rinterpolate_counter_t line = 0;
    while(line < table_to_interpolate->l)
    {
        rinterpolate_float_t * const x_table = coordinate_table->data + line * (n+d);
        rinterpolate_float_t * const r_table = x_table + n;

        printf("Line %4u : X (unmapped) ",line);
        for(rinterpolate_counter_t i = 0; i<n; i++)
        {
            printf("% 12g ",x_table[i]);
        }
        printf(" -> ");

        rinterpolate(
            latest_table->data,
            rinterpolate_data,
            latest_table->n,
            latest_table->d,
            latest_table->l,
            x_table,
            r,
            0);

        rinterpolate_float_t maxerr = 0.0;
        for(rinterpolate_counter_t i = 0; i<d; i++)
        {
            rinterpolate_float_t err = r_table[i] != 0.0 ? (100.0*fabs(1.0-r[i]/r_table[i])) : 0.0;
            printf("% 12g = % 12g (% 7.2f %%) %c ",
                   r_table[i], /* original table data */
                   r[i], /* interpolated data */
                   err,
                   i!=d-1?',':' ');
            maxerr = Max(err,maxerr);
        }
        printf("\n");

        if(maxerr > 10.0)
        {
            printf("ERR TOO LARGE\n");
            fflush(NULL);
            exit(0);
        }
        line++;
    }

    /* coordinate mapping needs to know that we have swapped columns,
     * so we need to introduce a swap history */

    /* can we just swap back and resort? maybe! */

    /* but does the sort also set the min max map? */
    Safe_free(r);
}

static void MAYBE_UNUSED test_map2(struct rinterpolate_data_t * rinterpolate_data)
{

    /*
     * make random data table :
     * n = number of parameters,
     * d = number of data
     */
    enum{ n = 3 };
    enum{ d = 3 };



    /*
     * number of points in each dimension
     * if >0 : use this many from a fixed array
     * if <0 : use this many random numbers
     */
    const rinterpolate_signed_counter_t nx[n] = {3,3,3};

    struct rinterpolate_table_t * latest_table = NULL;
    struct rinterpolate_table_t * original_table;

    /*
     * Build the table
     */
    if(0)
    {
        original_table = rinterpolate_random_table(
            rinterpolate_data,
            n,
            d,
            nx,
            __function
            );
    }
    if(1)
    {
        rinterpolate_float_t min[n] = {
            0.0,0.0,0.0
        };
        rinterpolate_float_t max[n] = {
            10.0,20.0,30.0
        };
        original_table = rinterpolate_linear_table(
            rinterpolate_data,
            n,
            d,
            min,
            max,
            nx,
            __function
            );
        rinterpolate_show_table(rinterpolate_data,
                                original_table,
                                stdout,
                                -1);
    }

    /*
     * Make a sorted version of the table for later checks
     */
    struct rinterpolate_table_t * sorted_original_table =
        rinterpolate_sort_table(rinterpolate_data,
                                original_table);


    printf("original table\n");
    rinterpolate_show_table(rinterpolate_data,
                            original_table,
                            stdout,
                            10);

    /*
     * Map right columns
     */
    latest_table =
        rinterpolate_map_right_columns(rinterpolate_data,
                                       original_table,
                                       (rinterpolate_Boolean_t []){
                                           /* which columns to map */
                                           FALSE, TRUE, TRUE
                                       },
                                       (rinterpolate_float_t []){
                                           /* mapped resolution */
                                           3,3,3
                                       },
                                       NULL);

    printf(" ... is table %d\n",
           rinterpolate_id_table(rinterpolate_data,
                                 latest_table->data));
    rinterpolate_show_table(rinterpolate_data,
                            latest_table,
                            stdout,
                            10);

    /*
     * now test how accurate the mapping is
     */
    const rinterpolate_counter_t number_of_random_tests = 0;
    rinterpolate_counter_t random_counter = 0;
    double * r = Rinterpolate_malloc(d * sizeof(rinterpolate_float_t));

    while(random_counter++ < number_of_random_tests)
    {
        double * x = Rinterpolate_malloc(n * sizeof(rinterpolate_float_t));
        printf("Interpolate at : ");
        for(rinterpolate_counter_t i = 0; i<n; i++)
        {
            x[i] = rinterpolate_random_coordinate(i);
            printf("%g ",x[i]);
        }
        printf(" -> ");

        rinterpolate(
            latest_table->data,
            rinterpolate_data,
            latest_table->n,
            latest_table->d,
            latest_table->l,
            x,
            r,
            0);
        for(rinterpolate_counter_t i = 0; i<d; i++)
        {
            printf("%g ",r[i]);
        }
        printf("\n");
        Safe_free(x);
    }

    printf("\n\nTry to interpolate the mapped table using pre-mapped coordinates  \n\n\n");

    struct rinterpolate_table_t * coordinate_table =
        sorted_original_table;
    struct rinterpolate_table_t * table_to_interpolate =
        latest_table;

    rinterpolate_counter_t line = 0;
    while(line < table_to_interpolate->l)
    {
        rinterpolate_float_t * const x_table = coordinate_table->data + line * (n+d);
        rinterpolate_float_t * const r_table = x_table + n;

        printf("Line %4u : X (unmapped) ",line);
        for(rinterpolate_counter_t i = 0; i<n; i++)
        {
            printf("% 12g ",x_table[i]);
        }
        printf(" -> ");

        rinterpolate(
            latest_table->data,
            rinterpolate_data,
            latest_table->n,
            latest_table->d,
            latest_table->l,
            x_table,
            r,
            0);

        rinterpolate_float_t maxerr = 0.0;
        for(rinterpolate_counter_t i = 0; i<d; i++)
        {
            rinterpolate_float_t err = r_table[i] != 0.0 ? (100.0*fabs(1.0-r[i]/r_table[i])) : 0.0;
            printf("% 12g = % 12g (% 7.2f %%) %c ",
                   r_table[i], /* original table data */
                   r[i], /* interpolated data */
                   err,
                   i!=d-1?',':' ');
            maxerr = Max(err,maxerr);
        }
        printf("\n");

        if(maxerr > 10.0)
        {
            printf("ERR TOO LARGE\n");
            fflush(NULL);
            exit(0);
        }
        line++;
    }

    /* coordinate mapping needs to know that we have swapped columns,
     * so we need to introduce a swap history */

    /* can we just swap back and resort? maybe! */

    /* but does the sort also set the min max map? */
    Safe_free(r);
}



static void test_rect(struct rinterpolate_data_t * rinterpolate_data)
{
    const size_t n = 3;
    const size_t d = 2;

    /*
     * Start with a stupidly disordered table
     */
    rinterpolate_float_t disordered_table_data[] =
    {
        0.1, 1.0, 1.0, 1.0, 1e1,
        0.1, 1.0, 2.0, 2.0, 1e2,
        0.1, 1.0, 10.0, 10.0, 1e10,
        0.1, 1.0, 15.0, 15.0, 1e15,
        0.1, 2.0, 1.0, 1.0, 1e1,

        0.2, 3.0, 1.0, 1.0, 1e1,
        0.2, 3.0, 2.0, 2.0, 1e2,
        0.2, 3.0, 10.0, 10.0, 1e10,
        0.2, 3.0, 15.0, 15.0, 1e15,

        0.1, 2.0, 2.0, 2.0, 1e2,
        0.1, 2.0, 10.0, 10.0, 1e10,
        0.1, 2.0, 15.0, 15.0, 1e15,
        0.1, 3.0, 1.0, 1.0, 1e1,
        0.1, 3.0, 2.0, 2.0, 1e2,
        0.1, 3.0, 10.0, 10.0, 1e10,
        0.1, 3.0, 15.0, 15.0, 1e15,


        0.2, 1.0, 1.0, 1.0, 1e1,
        0.2, 1.0, 2.0, 2.0, 1e2,
        0.2, 1.0, 10.0, 10.0, 1e10,
        0.2, 1.0, 15.0, 15.0, 1e15,
        0.2, 2.0, 1.0, 1.0, 1e1,
        0.2, 2.0, 2.0, 2.0, 1e2,
        0.2, 2.0, 10.0, 10.0, 1e10,
        0.2, 2.0, 15.0, 15.0, 1e15,
    };

    /* number of lines in the table */
    const size_t l = sizeof(disordered_table_data) /
        sizeof(rinterpolate_float_t) / (n+d);

    /* coordinate for interpolation in original space */
    const rinterpolate_float_t x[] =
    {
        0.1, 2.5, 10
    };

    rinterpolate_float_t * r = Rinterpolate_malloc(d*sizeof(rinterpolate_float_t));

    const rinterpolate_counter_t disordered_table_number =
        rinterpolate_add_new_table(
            rinterpolate_data,
            disordered_table_data,
            n,
            d,
            l,
            0,
            FALSE);

    /*
     * Make unsorted table
     */
    struct rinterpolate_table_t * const unsorted_disordered_table =
        rinterpolate_data->tables[disordered_table_number];

    /*
     * Make sorted table
     */
    struct rinterpolate_table_t * const
        disordered_table = rinterpolate_sort_table(rinterpolate_data,
                                                   unsorted_disordered_table);

    rinterpolate_show_table(rinterpolate_data,
                            unsorted_disordered_table,
                            stdout,
                            -1);
    rinterpolate_show_table(rinterpolate_data,
                            disordered_table,
                            stdout,
                            -1);

    printf("Is unsorted table orthogonal? %d\n",
           rinterpolate_is_table_orthogonal(rinterpolate_data,
                                            unsorted_disordered_table,
                                            FALSE));
    printf("Is sorted table orthogonal? %d\n",
           rinterpolate_is_table_orthogonal(rinterpolate_data,
                                            disordered_table,
                                            FALSE));

    for(rinterpolate_counter_t j=0; j<n-1; j++)
    {
        printf("col %u : min %g max %g : nvalues %u\n",
               j,
               disordered_table->stats->min[j],
               disordered_table->stats->max[j],
               disordered_table->stats->nvalues[j]
            );
        for(rinterpolate_counter_t k=0;
            k < disordered_table->stats->nvalues[j];
            k++)
        {
            printf("  %g -> [ %g to %g ]\n",
                   disordered_table->stats->valuelist[j][k],
                   disordered_table->stats->valuemin[j][k],
                   disordered_table->stats->valuemax[j][k]
                );
        }
        printf("]\n");
    }

    /* table is now ordered */
    struct rinterpolate_table_t * latest_table =
        rinterpolate_map_right_column(
            rinterpolate_data,
            disordered_table,
            10, // new resolution
            NULL // make resolution list automatically
            );


    rinterpolate_show_table(rinterpolate_data,
                            latest_table,
                            stdout,
                            -1);
    printf("Is mapped table orthogonal? %s\n",
           rinterpolate_is_table_orthogonal(rinterpolate_data,
                                            latest_table,
                                            FALSE) ? "Y" : "N");

    if(0)
    {
        /*
         * Swap columns and replace table
         */
        rinterpolate_swap_columns(rinterpolate_data,
                                  latest_table,
                                  1,
                                  2,
                                  FALSE);
        printf("Swapped\n");
        rinterpolate_show_table(rinterpolate_data,
                                latest_table,
                                stdout,
                                -1);
        printf("Is swapped table orthogonal? %d\n",
               rinterpolate_is_table_orthogonal(rinterpolate_data,
                                                latest_table,
                                                FALSE));
        struct rinterpolate_table_t * sorted_latest_table =
            rinterpolate_sort_table(rinterpolate_data,
                                    latest_table);
        rinterpolate_show_table(rinterpolate_data,
                                sorted_latest_table,
                                stdout,
                                -1);
        printf("Is sorted swapped table orthogonal? %d\n",
               rinterpolate_is_table_orthogonal(rinterpolate_data,
                                                sorted_latest_table,
                                                FALSE));
        latest_table = sorted_latest_table;
    }

    {
        /*
         *  map coords for logging (also done in rinterpolate())
         */
        rinterpolate_float_t * newx =
            rinterpolate_map_coordinate(rinterpolate_data,
                                        latest_table,
                                        x);

        printf("X    %20g %20g %20g\n",   x[0],   x[1],   x[2]);
        printf("NEWX %20g %20g %20g\n",newx[0],newx[1],newx[2]);

        Rinterpolate(latest_table,x,r,0);

        /*
         * Show result of the interpolation
         */
        printf("INTERPOLATE mapped : ");
        for(rinterpolate_counter_t i = 0; i<n; i++)
        {
            printf("%g->%g ",x[i],newx[i]);
        }
        printf(": ");
        for(rinterpolate_counter_t i = 0; i<d; i++)
        {
            printf("%g ",r[i]);
        }
        printf("\n");

        Safe_free(newx);
    }


    /*
     * Interpolate each line of the table : do we get back what
     * we started with?
     */
    for(rinterpolate_counter_t i = 0; i<l; i++)
    {
        rinterpolate_float_t * xx = Rinterpolate_malloc(sizeof(rinterpolate_float_t)*n);
        rinterpolate_float_t * rr = Rinterpolate_malloc(sizeof(rinterpolate_float_t)*d);

        memcpy(xx,
               disordered_table->data + i * latest_table->line_length,
               sizeof(rinterpolate_float_t)*n);

        Rinterpolate(latest_table,
                     xx,
                     rr,
                     0);

        printf("At ");
        for(rinterpolate_counter_t j = 0; j<n; j++)
        {
            printf("%12g ",xx[j]);
        }
        printf("  :  ");
        for(rinterpolate_counter_t j = 0; j<d; j++)
        {
            printf("%12g ",rr[j]);
        }
        printf("\n");

        Safe_free(xx);
        Safe_free(rr);
    }


    /*
     * Free data allocated here
     */
    Safe_free(r);


    /*
     * Test swap function
     */
    rinterpolate_float_t test_swap_data[] =
    {
        1, 10, 100,
        2, 20, 200,
        3, 30, 300,
        4, 40, 400
    };

    const rinterpolate_counter_t swap_table_number =
        rinterpolate_add_new_table(
            rinterpolate_data,
            test_swap_data,
            3,
            0,
            4,
            0,
            FALSE);
    struct rinterpolate_table_t * const swap_table =
        rinterpolate_data->tables[swap_table_number];

    printf("Pre swap\n");
    for(rinterpolate_counter_t i = 0; i<4; i++)
    {
        printf("%g %g %g\n",
               *(swap_table->data + 3*i + 0),
               *(swap_table->data + 3*i + 1),
               *(swap_table->data + 3*i + 2));
    }

#define TEST_READ_WRITE
#ifdef TEST_READ_WRITE
    {

    double data[] = {
        1, 10, 1,
        1, 20, 1.5,
        2, 20, 2,
        2, 30, 2.5,
        3, 30, 3,
        3, 40, 3.5
    };
    rinterpolate_add_new_table(
        rinterpolate_data,
        data,
        2,
        1,
        6,
        0,
        FALSE
        );

    struct rinterpolate_table_t * t =  rinterpolate_table_struct(rinterpolate_data,
                                                                 data,
                                                                 NULL);

    struct rinterpolate_table_t * tmapped = rinterpolate_map_right_columns(
        rinterpolate_data,
        t,
        (rinterpolate_Boolean_t[]){FALSE,TRUE},
        (rinterpolate_float_t[]){10,10},
        NULL
        );

    printf("MAPPED %p\n",(void*)tmapped);fflush(NULL);

    rinterpolate_show_table(rinterpolate_data,
                            tmapped,
                            stdout,
                            0);

    FILE * fp = fopen("/tmp/table.test","w");
    rinterpolate_write_table_to_file(tmapped,fp);
    fclose(fp);

    fp = fopen("/tmp/table.test","r");

    struct rinterpolate_table_t * t2 = rinterpolate_read_table_from_file(rinterpolate_data,
                                                                         fp);
    fclose(fp);
    rinterpolate_show_table(rinterpolate_data,
                            t2,
                            stdout,
                            0);

    }
#endif//TEST_READ_WRITE
}

static void test_free(struct rinterpolate_data_t ** rinterpolate_data)
{
    rinterpolate_free_data(*rinterpolate_data);
    Safe_free(*rinterpolate_data);
}

static void test_exit(struct rinterpolate_data_t * rinterpolate_data)
{
    test_free(&rinterpolate_data);
    exit(0);
}

#endif //__TEST_RINTERPOLATE__

typedef int prevent_ISO_warning;
