#include "rinterpolate.h"
#include "rinterpolate_internal.h"

struct rinterpolate_table_t *
rinterpolate_copy_table(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                        const struct rinterpolate_table_t * const in)
{
    /*
     * Function to make a copy of table "in".
     *
     * If rinterpolate_data is non-NULL, put the new
     * table in rinterpolate_data, otherwise just
     * make it and return it.
     *
     * Return a pointer to the new table, or NULL on error.
     */
    struct rinterpolate_table_t * out;

    /*
     * Set up copy of the data
     */
    const size_t size = sizeof(rinterpolate_float_t) * ((in->n + in->d) * in->l);
    rinterpolate_float_t * const data = Rinterpolate_malloc(size);
    if(!data) return NULL;

    memcpy(data,in->data,size);

    if(rinterpolate_data)
    {
        const rinterpolate_counter_t n =
            rinterpolate_add_new_table(rinterpolate_data,
                                       data,
                                       in->n,
                                       in->d,
                                       in->l,
                                       0,
                                       FALSE);
        out = rinterpolate_data->tables[n];
    }
    else
    {
        out = rinterpolate_new_table(rinterpolate_data,
                                     data,
                                     in->n,
                                     in->d,
                                     in->l,
                                     0,
                                     FALSE);
    }

    if(out)
    {
        out->auto_free_data = TRUE;
        rinterpolate_copy_table_data(in,
                                     out);
    }

    return out;
}
