#include "rinterpolate.h"
#include "rinterpolate_internal.h"

#ifdef RINTERPOLATE_CACHE

void rinterpolate_alloc_cacheline(struct rinterpolate_data_t * RESTRICT const rinterpolate_data MAYBE_UNUSED,
                                  struct rinterpolate_table_t * RESTRICT const table)
{
    /*
     * Allocate cache space for this interpolation table
     */
#ifdef RINTERPOLATE_DEBUG
    Rinterpolate_print("Allocated new cache array for table_id=%u\n",
           table->table_number);
#endif
    table->cache_match_line = 0;
    table->cache_spin_line  = -1;
    table->cache =
        Rinterpolate_calloc(table->line_length*table->cache_length,
                            sizeof(rinterpolate_float_t));
    if(unlikely(table->cache==NULL))
    {
        rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                           "Failed to alloc cache in table %p with parent %p \n",
                           rinterpolate_data,
                           (void*)table,
                           (void*)table->parent);
    }
}
#endif //RINTERPOLATE_CACHE
