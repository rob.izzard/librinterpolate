#include "rinterpolate.h"
#include "rinterpolate_internal.h"
/*
 * rinterpolate_free_data
 *
 * Given a rinterpolate_data struct, free everything in it.
 *
 * Note: we do not free the rinterpolate_data struct.
 */

void rinterpolate_free_data(struct rinterpolate_data_t * RESTRICT const rinterpolate_data)
{
    if(rinterpolate_data)
    {
        /*
         * Free nested, e.g. min_max, tables first
         */
        for(rinterpolate_counter_t i=0;i<rinterpolate_data->number_of_interpolation_tables;i++)
        {
            if(rinterpolate_data->tables[i] != NULL &&
               rinterpolate_data->tables[i]->min_max_table != NULL)
            {
                for(rinterpolate_counter_t j=0; j<rinterpolate_data->tables[i]->n; j++)
                {
                    if(rinterpolate_data->tables[i]->min_max_table[j] != NULL)
                    {
                        rinterpolate_free_table_contents(rinterpolate_data->tables[i]->min_max_table[j]);
                        Safe_free(rinterpolate_data->tables[i]->min_max_table[j]);
                    }
                }
            }
        }

        /*
         * Free remaining tables
         */
        for(rinterpolate_counter_t i=0;i<rinterpolate_data->number_of_interpolation_tables;i++)
        {
            if(rinterpolate_data->tables[i] != NULL)
            {
                rinterpolate_free_table_contents(rinterpolate_data->tables[i]);
                Safe_free(rinterpolate_data->tables[i]);
            }
        }
        Safe_free(rinterpolate_data->tables);
        rinterpolate_data->number_of_interpolation_tables=0;
    }
}
