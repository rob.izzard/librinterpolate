#include "rinterpolate.h"
#include "rinterpolate_internal.h"

struct rinterpolate_table_t * rinterpolate_new_table(
    struct rinterpolate_data_t * const rinterpolate_data,
    const rinterpolate_float_t * RESTRICT const data,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_counter_t l,
    const rinterpolate_counter_t cache_length,
    const rinterpolate_Boolean_t analyse
    )
{
    /*
     * Make a new table and return a pointer to it.
     *
     * Note: this does not put the table into rinterpolate_data:
     *       that is done in rinterpolate_add_new_table()
     */
    struct rinterpolate_table_t * const table =
        Rinterpolate_calloc(1,sizeof(struct rinterpolate_table_t));

    if(table)
    {
        rinterpolate_new_table_contents(
            rinterpolate_data,
            table,data,n,d,l,cache_length,analyse
            );
    }
    else
    {
        rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                           "Failed to allocate space for table of size %zu\n",
                           rinterpolate_data,
                           sizeof(struct rinterpolate_table_t));
    }

    return table;
}
