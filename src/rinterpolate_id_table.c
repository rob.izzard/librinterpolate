#include "rinterpolate.h"
#include "rinterpolate_internal.h"
/*
 * Attempt to match a table to those in the rinterpolate
 * data structure.
 */

rinterpolate_signed_counter_t Pure_function rinterpolate_id_table(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    const rinterpolate_float_t * RESTRICT const data
    )
{
    /* look for data table in the existing table_ids */
    rinterpolate_Boolean_t found = FALSE;

    if(rinterpolate_data == NULL ||
       data == NULL)
    {
        Rinterpolate_print("rinterpolate_data = %p, data = %p : both should be non-NULL\n",
                           (void*)rinterpolate_data,
                           (void*)data);
        return -1;
    }

    Rinterpolate_print("Look for table in existing table ids\n");
    Rinterpolate_print("currently have %u tables\n",
                       rinterpolate_data->number_of_interpolation_tables);

    rinterpolate_counter_t table_num = 0;
    while(table_num < rinterpolate_data->number_of_interpolation_tables)
    {
        if(rinterpolate_data->tables[table_num] != NULL &&
           rinterpolate_data->tables[table_num]->data ==
           (rinterpolate_float_t *)data)
        {
            /* found : break out of loop and use it */
            found = TRUE;
            break;
        }
        table_num++;
    }

    /* not found */
    if(found == FALSE)
    {
        /* if not found, allocate */
        Rinterpolate_print("Not found\n");
        table_num = -1;
    }
    else
    {
        Rinterpolate_print("Found at %u\n",table_num);
        Rinterpolate_print("Table %u label %s ",
                           table_num,
                           rinterpolate_data->tables[table_num]->label);
    }
    return table_num;
}
