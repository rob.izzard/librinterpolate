#include "rinterpolate.h"
#include "rinterpolate_internal.h"

/*
 * Given a pointer to a set of data, find a matching table
 * rinterpolate_data and free it if it is there.
 *
 * Nested min_max tables are also freed.
 */

void rinterpolate_free_table_from_data(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                       void * const data)
{
    if(rinterpolate_data != NULL &&
       data != NULL)
    {
        for(rinterpolate_counter_t i=0;
            i<rinterpolate_data->number_of_interpolation_tables;
            i++)
        {
            struct rinterpolate_table_t * const table = rinterpolate_data->tables[i];
            if(likely(table != NULL) &&
               table->data == data)
            {
                if(table->min_max_table != NULL)
                {
                    for(rinterpolate_counter_t j=0; j<table->n; j++)
                    {
                        if(table->min_max_table[j] != NULL)
                        {
                            rinterpolate_free_table_contents(table->min_max_table[j]);
                            Safe_free(table->min_max_table[j]);
                        }
                    }
                }
                rinterpolate_free_table_contents(table);
                Safe_free(rinterpolate_data->tables[i]);
            }
        }
    }
}
