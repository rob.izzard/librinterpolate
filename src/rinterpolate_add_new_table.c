#include "rinterpolate.h"
#include "rinterpolate_internal.h"

rinterpolate_counter_t rinterpolate_add_new_table(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    const rinterpolate_float_t * RESTRICT const data,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_counter_t l,
    const rinterpolate_counter_t cache_length,
    const rinterpolate_Boolean_t analyse
    )
{
    /*
     * Allocate memory using rinterpolate_new_table,
     * and return the new table number.
     */
    struct rinterpolate_table_t * const RESTRICT table = rinterpolate_new_table(
        rinterpolate_data,
        data,
        n,
        d,
        l,
        cache_length,
        analyse
        );

    return rinterpolate_add_new_table_from_pointer(rinterpolate_data,
                                                   table);
}
