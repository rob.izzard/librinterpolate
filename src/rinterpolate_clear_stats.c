#include "rinterpolate.h"
#include "rinterpolate_internal.h"
/*
 * rinterpolate_clear_stats
 *
 * Given a rinterpolate table, free any associated stats,
 * but do not free the stats struct itself.
 */

void rinterpolate_clear_stats(struct rinterpolate_table_t * RESTRICT const table)
{
    if(table != NULL)
    {
        if(table->stats != NULL)
        {
            if(table->stats)
            {
                for(rinterpolate_counter_t j=0;j<table->n;j++)
                {
                    Safe_free(table->stats->valuelist[j]);
                    Safe_free(table->stats->valuemin[j]);
                    Safe_free(table->stats->valuemax[j]);
                }
            }

            Safe_free(table->stats->valuelist);
            Safe_free(table->stats->nvalues);
            Safe_free(table->stats->min);
            Safe_free(table->stats->max);
            Safe_free(table->stats->valuemin);
            Safe_free(table->stats->valuemax);
            Safe_free(table->stats->valuelist_alloced);
        }
        table->analysed = FALSE;
    }
}
