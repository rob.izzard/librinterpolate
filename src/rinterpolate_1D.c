#include "rinterpolate.h"
#include "rinterpolate_internal.h"


rinterpolate_float_t * rinterpolate_1D(
    const rinterpolate_float_t * const coords,
    const rinterpolate_float_t * const data,
    const rinterpolate_float_t x,
    const rinterpolate_counter_t nlines,
    const rinterpolate_counter_t d,
    const rinterpolate_counter_t offset)
{
    /*
     * Simple linear 1D interpolation of data in (sorted array)
     * coords at x.
     *
     * Returns a pointer to an array of length d containing the
     * interpolated data.
     *
     * d is the number of data items.
     *
     * offset can be:
     *
     * 0 : no coordinate in the first column of the data
     * 1 : coordinate in the first column of the data
     *
     * On error calls rinterpolate_error() then returns NULL.
     */
    const rinterpolate_Boolean_t vb = FALSE;
    if(vb)
    {
        printf("1D int\n");
        for(rinterpolate_counter_t i=0; i<nlines; i++)
        {
            printf("%g : ",coords[i]);
            for(rinterpolate_counter_t j=0; j<d; j++)
            {
                printf("%g ",*(data + i * (1+d) + j));
            }
            printf("\n");
        }
    }

    /*
     * Returned data
     */
    rinterpolate_float_t * const p =
        Rinterpolate_malloc(sizeof(rinterpolate_float_t)*d);

    if(p == NULL)
    {
        rinterpolate_error(
            RINTERPOLATE_MALLOC_FAILED,
            "malloc failed wanted %zu * %u = %zu bytes\n",
            NULL,
            sizeof(rinterpolate_float_t),
            d,
            sizeof(rinterpolate_float_t)*d
            );
        return NULL;
    }

    /*
     * True line length
     */
    const rinterpolate_counter_t lnl = offset + d;

    if(nlines==1)
    {
        /* only one line : must always match it */
        memcpy(p,
               data,
               sizeof(rinterpolate_float_t)*d);
    }
    else if(x < coords[0])
    {
        /* off lower end */
        memcpy(p,
               data + 0*lnl + offset,
               sizeof(rinterpolate_float_t)*d);
    }
    else if(x > coords[nlines-1])
    {
        /* off upper end */

        memcpy(p,
               data + (nlines-1)*lnl + offset,
               sizeof(rinterpolate_float_t)*d);
    }
    else
    {
        /* intermediate point : interpolate */
        rinterpolate_counter_t a = 0;
        rinterpolate_counter_t b = nlines;

        if(likely(b>1))
        {
            while(likely(b-a > 1))
            {
                const rinterpolate_counter_t c = (a+b)>>1;
                if(x > *(coords + c))
                {
                    a = c;
                }
                else
                {
                    b = c;
                }
            }
        }
        const rinterpolate_float_t f =
            (x - coords[a]) / (coords[b] - coords[a]);

        const rinterpolate_float_t f1 = 1.0 - f;
        const rinterpolate_float_t * const offset_data = data + offset;
        const rinterpolate_float_t * const left_data = offset_data + a*lnl;
        const rinterpolate_float_t * const right_data = offset_data + b*lnl;

        for(rinterpolate_counter_t i = 0; i < d; i++)
        {
            const rinterpolate_float_t left =
                *(left_data + i);
            const rinterpolate_float_t right =
                *(right_data + i);

            p[i] = f1 * left + f * right;
#ifdef RINTERPOLATE_NANCHECKS
            if(isnan(p[i]))
            {
                fprintf(stderr,
                        "rinterpolate_1D: p[i=%u (<d=%u)] is NAN from f1=%g * left=%g + f=%g * right=%g\n",
                        i,
                        d,
                        f1,
                        left,
                        f,
                        right
                    );
                printf("NAN\n");
                fflush(NULL);
                return NULL;
            }
#endif // RINTERPOLATE_NANCHECKS
        }
    }

    return p;
}
