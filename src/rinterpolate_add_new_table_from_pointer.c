#include "rinterpolate.h"
#include "rinterpolate_internal.h"

rinterpolate_counter_t rinterpolate_add_new_table_from_pointer(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    struct rinterpolate_table_t * RESTRICT const table
    )
{
    /*
     * Increase size of list of table_numbers, and store the appropriate pointer
     * to the table of data
     */
    const rinterpolate_counter_t table_number =
        rinterpolate_data->number_of_interpolation_tables;

    rinterpolate_data->number_of_interpolation_tables++;

    rinterpolate_data->tables =
        Rinterpolate_realloc(rinterpolate_data->tables,
                             sizeof(struct rinterpolate_table_t *) * rinterpolate_data->number_of_interpolation_tables);

    if(unlikely(rinterpolate_data->tables == NULL))
    {
        rinterpolate_error(RINTERPOLATE_REALLOC_FAILED,
                           "Realloc of rinterpolate_data->tables failed and returned NULL\n",
                           rinterpolate_data);
    }

    rinterpolate_data->tables[table_number] = table;

    /*
     * Set data pointers and table number
     */
    table->parent = rinterpolate_data;
    table->table_number = table_number;

    return table_number;
}
